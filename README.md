# Prueba técnica de Cleverpy

Prueba realizada por Alejandro Vañó Tomás para la empresa Cleverpy.

## Tecnologías

* React
* TypeScript


## Scripts

Comandos que se pueden ejecutar

### `npm start`

Corre la app en modo desarrollo.\
Abre [http://localhost:3000](http://localhost:3000) para ver en el navegador.

### `npm run build`

Genera la aplicación para producción en la carpeta `build`,\

## TODO

* Poner redux
* Hacer un login
* Tener los comentarios de los posts
* Hacer responsive para movil
* Dockerizar