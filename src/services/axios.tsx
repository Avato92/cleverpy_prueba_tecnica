import Response from '../models/Response';
import axios from 'axios';

export default class AxiosService {
    private static baseURL: string = 'https://jsonplaceholder.typicode.com';


    public static async getAll<T>(url: string): Promise<Response> {
        let res = await axios.get<Array<T>>(this.baseURL + url)
            .then(response => {
                return new Response(true, response.data as Array<T>, 'Éxito', '');
            })
            .catch(function (error) {
                return new Response(false, null, 'Error', error);
            });
        return res;
    }

    public static get<T>(url: string, param: any): Promise<Response> {
        let res = axios.get<T>(this.baseURL + url + '/' + param)
            .then(response => {
                return new Response(true, response.data, 'Éxito', '');
            })
            .catch(function (error) {
                return new Response(false, null, 'Error', error);
            });
        return res;
    }
    public static delete(url: string, param: any): Promise<Response> {
        let res = axios.post(this.baseURL + url + '/' + param)
            .then(response => {

                return new Response(true, null, 'Éxito', '');
            })
            .catch(function (error) {
                return new Response(false, null, 'Error', error);
            });
        return res;
    }
}