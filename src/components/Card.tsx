import { FaTrashAlt } from 'react-icons/fa';
import { CardProps } from '../models/Main';


function Card(props: CardProps){
    const {post, users, post_index} = props;
    
    return (
        <div>
            <div className='postContainer__box' key={post_index}>
            <div className='postContainer__titleSection'>
                <h3 className='postContainer__title'>{post.title}</h3>
            </div>
            <div className='postContainer__body'>
                <p className='postContainer__text'>{post.body}</p>
            </div>
            <div className='postContainer__bottom'>
                <strong>{users.map((user) => user.id === post.userId ? user.name: null)}</strong>
                <button className='postContainer__button postContainer__button-delete' onClick={() => props.onDeletePost(post.id)}><FaTrashAlt /></button>
            </div>
        </div>
        </div>

    )
}

export default Card;