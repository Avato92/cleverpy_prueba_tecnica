import './Loading.css';

function Loading() {
  return (
    <div className='loading'>
      <span className='loading__word loading__word-1'>L</span>
      <span className='loading__word loading__word-2'>o</span>
      <span className='loading__word loading__word-3'>a</span>
      <span className='loading__word loading__word-4'>d</span>
      <span className='loading__word loading__word-5'>i</span>
      <span className='loading__word loading__word-6'>n</span>
      <span className='loading__word loading__word-7'>g</span>
      <span className='loading__word loading__word-8'>.</span>
      <span className='loading__word loading__word-9'>.</span>
      <span className='loading__word loading__word-10'>.</span>
    </div>
  );
}

export default Loading;