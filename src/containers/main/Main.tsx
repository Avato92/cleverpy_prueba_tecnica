import { FunctionComponent, useState, useEffect } from 'react';
import axiosService from '../../services/axios';
import { Posts, Users } from '../../models/Main';
import './Main.css';
import Card from '../../components/Card';
import Loading from '../../components/Loading/Loading';

const Main: FunctionComponent<{}> = () => {

    const [posts, setPosts] = useState<Posts[]>([]);
    const [users, setUsers] = useState<Users[]>([]);
    const [loading, setLoading] = useState<Boolean>(true);

    useEffect(() => {
        axiosService.getAll<Posts>('/posts').then(
            res => {
                setPosts(res.Data);
                setLoading(false);
            }
        )
        axiosService.getAll<Users>('/users').then(
            res =>
                setUsers(res.Data)
        )
    },[])

    const onDeletePost = (postId: number) => {
        setPosts(posts.filter(post => post.id !== postId))
    }

    return (

        
        <div className='postContainer'>

            {loading ? 
                <Loading />
            :
                posts.map((post, i) => 
                    <Card post={post} post_index={i} users={users} onDeletePost={onDeletePost} />
                )}
        </div>
        
    )
}
export default Main;