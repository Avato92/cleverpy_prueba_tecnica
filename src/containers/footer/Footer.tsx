import { FunctionComponent } from 'react';
import './Footer.css';
import {IconContext} from 'react-icons';
import { FaLinkedin, FaGitlab, FaGithub } from 'react-icons/fa';


const Footer: FunctionComponent<{}> = () => {
    return (
        <div className='footer'>
            <section className='footer__personal'>
                <p className='footer__text footer__text-strong'>Alejandro Vañó Tomás</p>
                <p className='footer__text'>avato92@gmail.com</p>
            </section>
            <section className='footer__comment'>
                <p className='footer__text'>
                    &copy; 2021 - 2021 Prueba técnica de React con TypeScript para Cleverpy
                </p>

            </section>
            <section className='footer__social'>
                <IconContext.Provider value={{style:{fontSize: '30px', color: 'white'}}}>
                    <a href='https://www.linkedin.com/in/alejandrovato/'>
                        <FaLinkedin />
                    </a>
                    <a href='https://gitlab.com/Avato92' className='footer__link'>
                        <FaGitlab className='footer__link'/>
                    </a>
                    <a href='https://github.com/avato92'>
                        <FaGithub />
                    </a>
                </IconContext.Provider>
            </section>
            
            
        </div>
    )
}

export default Footer;