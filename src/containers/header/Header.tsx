import { FunctionComponent } from 'react';
import './Header.css';

const Header: FunctionComponent<{}> = () => {
    return (
        <nav className='header'>
            <h1 className='header__title'>Cleverpy</h1>
        </nav>
    )
}

export default Header;