import React from 'react';
import './App.css';
import Main from './containers/main/Main';
import Header from './containers/header/Header';
import Footer from './containers/footer/Footer';

function App() {
  return (
    <div className='App'>
      <Header />
      <Main />
      <Footer />
    </div>
  );
}

export default App;
