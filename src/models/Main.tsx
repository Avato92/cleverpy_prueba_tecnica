export type Posts = {
    userId: number,
    id: number,
    title: string,
    body: string
}

type Geo = {
    lat: string,
    lng: string
}

type Address = {
    street: string,
    suite: string,
    city: string,
    zipcode: string,
    geo: Geo
}

export type Users = {
    id: number,
    name: string,
    username: string,
    email: string,
    address: Address
}

export type CardProps = {
    post: Posts;
    users: Array<Users>;
    post_index: number;
    onDeletePost: (postId: number) => void;
}